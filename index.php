<?php
/**
 * Created by PhpStorm.
 * User: Makhnev
 * Date: 27.12.2016
 * Time: 12:44
 */


$url1 = 'http://funpay.ru';
$url2 = 'http://funpay.ru/yandex/emulator';

$postFields = [
    'receiver' => 410011946993408,
    'sum'=>45
];

$cookiePath = './cook.txt';

$userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36';

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url1);
//curl_setopt($ch, CURLOPT_POST, 1);
//curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postFields));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiePath);
curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiePath);
curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);


$response = curl_exec($ch);
$error    = curl_error($ch);
$errno    = curl_errno($ch);

curl_close ($ch);


//var_dump('1', $errno, $error);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url2);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postFields));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiePath);
curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiePath);
curl_setopt($ch, CURLOPT_REFERER, 'http://funpay.ru/yandex/emulator');
curl_setopt($ch, CURLOPT_USERAGENT, '');
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type:application/x-www-form-urlencoded; charset=UTF-8',
    'Accept:text/html, */*; q=0.01',
    'X-Requested-With: XMLHttpRequest'
]);

$options = array(
    CURLOPT_RETURNTRANSFER => true,   // return web page
    CURLOPT_HEADER         => false,  // don't return headers
    CURLOPT_FOLLOWLOCATION => true,   // follow redirects
    CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
    CURLOPT_ENCODING       => "",     // handle compressed
    CURLOPT_USERAGENT      => $userAgent, // name of client
    CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
    CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
    CURLOPT_TIMEOUT        => 120,    // time-out on response
);

curl_setopt_array($ch, $options);


$response = curl_exec($ch);
$error    = curl_error($ch);
$errno    = curl_errno($ch);

curl_close ($ch);

$phrases = ['Пароль:', 'Спишется ', 'Перевод на счет '];
$answers = [];

$rows = explode('<br />', $response);

function getPart($row, $passPhrase){
    if(strpos($row, $passPhrase) !== false){
        return substr($row, strlen($passPhrase));
    }
    return false;
}

foreach($rows as $row){
    foreach($phrases as $phrase){
        $answer = getPart($row, array_shift($phrases));
        if($answer){
            $answers[] = $answer;
            break;
        }
    }
}

var_dump($answers);die;